﻿using School.Data;
using System;
using System.Windows;
using System.Windows.Controls;

namespace School
{
    /// <summary>
    /// Interaction logic for StudentForm.xaml
    /// </summary>
    public partial class StudentForm : Window
    {
        // Field for tracking the currently selected student
        private Student student = null;

        public delegate void EditStudentEventHandler(bool edited, Student student);
        public event EditStudentEventHandler EditStudent;

        #region Predefined code

        public StudentForm()
        {
            InitializeComponent();
        }

        public StudentForm(Student student)
        {
            InitializeComponent();

            this.student = student;
            UpdateForm();
        }

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            EditStudent(true, GetEditedStudent());
            Close();
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #endregion

        private void UpdateForm()
        {
            Title = $"Detail of {student.FirstName}";

            firstName.Text = student.FirstName;
            lastName.Text = student.LastName;
            dateOfBirth.Text = student.DateOfBirth.ToString("yyyy-MM-dd");
        }

        private Student GetEditedStudent()
        {
            student.FirstName = firstName.Text;
            student.LastName = lastName.Text;
            try
            {
                student.DateOfBirth = Convert.ToDateTime(dateOfBirth.Text);
            }
            catch (FormatException ex)
            {
                MessageBox.Show("Your date of birth should follow this format 'yyyy-MM-dd'!", "Cannot get datetime", MessageBoxButton.OK, MessageBoxImage.Warning);
                student.DateOfBirth = student.DateOfBirth;
            }

            return student;
        }
    }
}
