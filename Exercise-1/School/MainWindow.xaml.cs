﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using School.Data;
using System.Globalization;
using System.Data;

namespace School
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Connection to the School database
        private SchoolDBEntities schoolContext = null;

        // Field for tracking the currently selected teacher
        private Teacher teacher = null;

        // List for tracking the students assigned to the teacher's class
        private IList studentsInfo = null;

        // Declare student form to diplay detail of student
        private StudentForm studentForm;

        #region Predefined code

        public MainWindow()
        {
            InitializeComponent();
        }

        // Connect to the database and display the list of teachers when the window appears
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.schoolContext = new SchoolDBEntities();
            teachersList.DataContext = this.schoolContext.Teachers;
        }

        // When the user selects a different teacher, fetch and display the students for that teacher
        private void teachersList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Find the teacher that has been selected
            this.teacher = teachersList.SelectedItem as Teacher;
            this.schoolContext.LoadProperty<Teacher>(this.teacher, s => s.Students);

            // Find the students for this teacher
            this.studentsInfo = ((IListSource)teacher.Students).GetList();

            // Use databinding to display these students
            studentsList.DataContext = this.studentsInfo;
        }

        #endregion

        // When the user presses a key, determine whether to add a new student to a class, remove a student from a class, or modify the details of a student
        private void studentsList_KeyDown(object sender, KeyEventArgs e)
        {
            // TODO: Exercise 1: Task 1a: If the user pressed Enter, edit the details for the currently selected student
            // TODO: Exercise 1: Task 2a: Use the StudentsForm to display and edit the details of the student
            // TODO: Exercise 1: Task 2b: Set the title of the form and populate the fields on the form with the details of the student
            // TODO: Exercise 1: Task 3a: Display the form
            // TODO: Exercise 1: Task 3b: When the user closes the form, copy the details back to the student
            // TODO: Exercise 1: Task 3c: Enable saving (changes are not made permanent until they are written back to the database)

            if (e.Key == Key.Enter)
            {
                GetStudentDetail(GetStudentSelectedFromList());
            }
        }

        #region Predefined code

        private void studentsList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            GetStudentDetail(GetStudentSelectedFromList());
        }

        // Save changes back to the database and make them permanent
        private void saveChanges_Click(object sender, RoutedEventArgs e)
        {
            SaveChanges();
        }

        #endregion


        private Student GetStudentSelectedFromList()
        {
            Student student = (Student) studentsList.SelectedItem;
            return student;
        }

        private void GetStudentDetail(Student student)
        {
            studentForm = new StudentForm(student);

            // Add event to update student list after edit student detail
            studentForm.EditStudent += new StudentForm.EditStudentEventHandler(UpdateStudentList);
            studentForm.ShowDialog();
        }

        private void UpdateStudentList(bool edited, Student student)
        {
            if (edited)
            {
                // Update selected student detail
                Student selectedStudent = GetStudentSelectedFromList();
                selectedStudent.FirstName = student.FirstName;
                selectedStudent.LastName = student.LastName;
                selectedStudent.DateOfBirth = student.DateOfBirth;

                studentsList.SelectedItem = selectedStudent;

                // Enable save changes button
                saveChanges.IsEnabled = true;
            }
        }

        private void SaveChanges()
        {
            // Update all student
            foreach(Student student in studentsList.Items)
            {
                schoolContext.Students.ApplyCurrentValues(student);
            }

            int result = schoolContext.SaveChanges();

            if(result > 0)
            {
                MessageBox.Show("Save changes successfully!", "Save changes", MessageBoxButton.OK, MessageBoxImage.Information);
                saveChanges.IsEnabled = false;
            }
        }

    }

    [ValueConversion(typeof(string), typeof(Decimal))]
    class AgeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
                              System.Globalization.CultureInfo culture)
        {
            DateTime dateOfBirth = (DateTime) value;
            int currentYear = DateTime.Now.Year;
            return (currentYear - dateOfBirth.Year).ToString();
        }

        #region Predefined code

        public object ConvertBack(object value, Type targetType, object parameter,
                                  System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
